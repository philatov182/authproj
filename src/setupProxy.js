const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    ['/v1/accounts*'],
    createProxyMiddleware({
      target: 'https://identitytoolkit.googleapis.com',
      changeOrigin: true
    })
  );
};