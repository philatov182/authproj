import React from "react";
import InputField from "./InputField";
//import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import axios from "axios";
import firebaseConfig from "./firebase-config";

class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          email: "",
          password: "",
          success: false,
          error: ""
        };
    }
    
    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        const {email, password} = this.state;
        
        /*const auth = getAuth();
        signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            this.setState({ success: true, error: ""});
        })
        .catch((error) => {
            this.setState({ success: false, error: error.message});
        });*/

        const url = process.env.REACT_APP_FIREBASE_AUTH_URL + firebaseConfig.apiKey;
        axios.post(url, JSON.stringify({email, password, returnSecureToken: false}),
            {
                headers: { 
                    "Content-Type": "application/json",
                },
                proxy: true
            })
            .then(_ => {
                this.setState({ success: true, error: ""});
            })
            .catch(e => {
                this.setState({ success: false, error: e.response.data.error.message});
            });
    }

    render() {
        const { email, success, error } = this.state;
        const successfulText = "Successful auth with email " + email;
        return (
            <div className="form">
                <h1>Log in</h1>
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <InputField placeHolder={"Email"} value={this.state.email} onChange={(e) => this.handleChangeEmail(e)}/>
                    <InputField placeHolder={"Password"} type={"password"} value={this.state.password} onChange={(e) => this.handleChangePassword(e)}/>
                    <InputField type={"submit"} value="Sign in"/>
                </form>
                <p>{success && !error ? successfulText : error}</p>
            </div>
          );
    }
}

export default Auth;
