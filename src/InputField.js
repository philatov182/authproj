import React from "react";

function InputField(props) {
    return (
        <pre>
            <input placeHolder={props.placeHolder} type={props.type || "text"} 
                value={props.value} onChange={props.onChange} required/>
        </pre>
      );
}

export default InputField;
